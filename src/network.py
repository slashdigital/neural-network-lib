import json
from .layer import Layer
from .layer import OutputLayer
import numpy

def image(ds):
	X = []
	Y = []
	for elm in ds:
		X.append(elm["data"])
		Y.append(elm["value"])
	return X, Y

def sigmoid(a):
	output = 1/ (1 + numpy.exp(-a))
	return output
def sigmoid_output_to_derivative(output):
	return output*(1-output)

def tanh(x):
    return numpy.tanh(x)

def tanh_deriv(x):
    return 1.0 - numpy.tanh(x)**2

def relu(data, epsilon=0.1):
    return numpy.maximum(epsilon * data, data)

def reluprime(x):
    return numpy.where(x > 0, 1.0, 0.0)

get_activ = {
	"tanh": (tanh, tanh_deriv),
	"sigmoid" : (sigmoid, sigmoid_output_to_derivative),
	"relu" : (relu, reluprime)
}

class Network(object):
	def __init__(self, dataset, config_file, model_maker=image,
	activation="sigmoid"):
		with open(config_file) as conf:
			config = json.load(conf)
		self.activation, self.activation_derivate = get_activ[activation]
		self.model_maker = model_maker
		self.hidden_layer = config["hidden_layers"]
		self.epochs = config["epochs"]
		self.alpha = config["alpha"]
		self.dataset = dataset

	def transform_data(self):
		self.model, self.expected = self.model_maker(self.dataset)

	def generate_layers(self, input):
		value = numpy.array(input)
		layers = []
		for i in range(len(self.hidden_layer) - 1):
			layers.append(Layer(self.synapses[i], value, self.activation,
								self.activation_derivate))
			value = layers[-1].forward()
		layers.append(OutputLayer(value, self.activation_derivate))
		return layers

	def update_synapses(self, layers):
		for i in range(len(self.synapses)):
			update = layers[i].value.T.dot(layers[i + 1].delta)
			self.synapses[i] -= self.alpha * update


	def backprob(self, layers, index):
		errors = []
		deltas = []
		error, delta = layers[-1].backward(layers[-1].value, self.expected)

		if index % 10 == 0:
			print("Error rate:", numpy.mean(numpy.abs(error)), "at Epoch number ", index)

		for i in reversed(range(1, len(self.hidden_layer) - 1)):
			error, delta = layers[i].backward(error, delta)
		return layers

	def init_synapses(self):
		layers_size = self.hidden_layer
		layers_size.insert(0, len(self.model[0])) # ADD size of input
		layers_size.append(len(self.expected[0])) # ADD size of output
		self.synapses = []
		numpy.random.seed(1)

		#Init Synapses values
		self.synapses.append(2 * numpy.random.rand(layers_size[0],layers_size[1]) - 1)
		for i in range(1, len(layers_size) - 1):
			self.synapses.append(2 * numpy.random.rand(layers_size[i],
			layers_size[i + 1]) - 1)

	def train(self):
		self.transform_data()
		#Transform the dataset to model
		self.init_synapses()
		for index in range(self.epochs):
			#Calcul of the layers
			layers = self.generate_layers(self.model)
			#BackPropagate
			deltas = self.backprob(layers, index)
			#Update the synapses values
			self.update_synapses(layers)

	def classify(self, X, already_made=True):
		if not already_made:
			X = self.model_maker(X)
		X = numpy.array(X)
		layers = self.generate_layers(X)
		return layers[-1].value

	def store(self, file):
		store = []
		for elm in self.synapses:
			store.append(elm.tolist())
		with open(file, "w") as f:
			json.dump(store, f, indent=4)

	def load(self, file):
		with open(file) as f:
			try:
				self.synapses.clear()
			except:
				self.synapses = []
			fjson = json.load(f)
			for elm in fjson:
				self.synapses.append(numpy.asarray(elm))

