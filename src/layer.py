import numpy

class Layer(object):
	def __init__(self, synapse, value, activation, derivate):
		self.synapse = synapse
		self.value = value
		self.activation = activation
		self.derivate = derivate
		self.delta = None
	def forward(self):
		return self.activation(numpy.dot(self.value, self.synapse))
	def backward(self, last_error, last_delta):
		error = last_delta.dot(self.synapse.T)
		delta = error * self.derivate(self.value)
		self.delta = delta
		return error, delta

class OutputLayer(Layer):
	def __init__(self, value, derivate):
		self.value = value
		self.derivate = derivate
		self.delta = None
	def backward(self, output, expected):
		error = output - expected
		delta = error * self.derivate(self.value)
		self.delta = delta
		return error, delta
	def forward(self):
		pass
