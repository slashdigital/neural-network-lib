# NNlib

This project is just a way to generate Neural Network using backpropagation algorithm

### Installing

```
git clone https://github.com/deltova/NNlib.git
pip3 install numpy
```

## Exemple
```
from network import Network
ds = [{"data" : [0, 1], "value" : [1]}
	  ,{"data" : [0, 0], "value" : [0]}
	  ,{"data" : [1, 0], "value" : [1]}]

net =  Network(ds, "test/config.json", activation="tanh")
net.train()
print(net.classify([1,1]))
```

## Configuration File
```
{
  "hidden_layers" : [20, 10],
  "epochs" : 100,
  "alpha" : 1,
}
```
The hidden_layers array corresponding to the size of each hidden layer you can
define as many hidden layer as you want.

The epochs field in the number of epochs you want to train the neural network

The alpha field is the value of the alpha

## Activation Function
You can use 3 differents activation function.
relu
sigmoid
tanh

## Differente Functionnalities
```
net.store("file_name")
```
Allow you to store your synapses in a file as json
```
net.load("file_name")
```
Allow you to load synapses from a file in json format


## Authors

Clement Magnard : https://github.com/deltova
